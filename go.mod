module gitee.com/yuyuaa/easy-go-tool

go 1.17

//require gofunc v0.0.0

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/llgcode/draw2d v0.0.0-20210904075650-80aa0a2a901d
	github.com/parnurzeal/gorequest v0.2.16
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/tatsushid/go-fastping v0.0.0-20160109021039-d7bb493dee3e
	golang.org/x/image v0.5.0
)

require (
	github.com/elazarl/goproxy v0.0.0-20221015165544-a0805db90819 // indirect
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/smartystreets/assertions v1.13.0 // indirect
	github.com/smartystreets/goconvey v1.7.2 // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	moul.io/http2curl v1.0.0 // indirect
)

//replace gofunc v0.0.0 => ./gofunc
