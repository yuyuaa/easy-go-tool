package model

import (
	"context"
	"time"
)

type littleDog struct {
	name           string
	dead           context.CancelFunc //告诉母狗这只狗死了
	food           chan int           //有食物喂他
	feedIntevalSec int                //最大容许的喂狗间隔
	exit           chan bool
}

//开始循环喂狗
func (self *littleDog) run() {
	go func() {
		for {
			select {
			case <-time.After(time.Duration(self.feedIntevalSec) * time.Second): //
				//喂狗
				//log.Println("小狗", self.name, "饿死了")
				self.dead() //告诉狗妈妈该小狗死掉了
				return
			case <-self.exit:
				return
			case <-self.food:
				//log.Println("喂小狗", self.name, "了")
			}
		}
	}()
}

//根据具体
func (self *littleDog) Feed() {
	self.food <- 1
}

//关闭这个狗
func (self *littleDog) Close() {
	self.exit <- true
}
