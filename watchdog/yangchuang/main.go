package yangchuang

import (
	"gitee.com/yuyuaa/easy-go-tool/watchdog/model"
	"gitee.com/yuyuaa/easy-go-tool/ycapi"
)

type Wdog struct {
	model.MotherDog
}

func New() *Wdog {
	wd := new(Wdog)
	wd.Init()
	err := ycapi.SetWDog(30) //初始化为30秒喂一次
	if err != nil {
		panic(err)
	}
	ycapi.StartWDog()
	wd.AutoFeed(10, func() { //实际为10秒喂一次
		ycapi.FeedWDog()
	}, func() {
		panic("测试重启")
	})
	return wd
}
