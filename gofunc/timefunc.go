package gofunc

import (
	"fmt"
	"time"
)

type TimeFunc struct {
}

// 当前时间是否在输入的时间点之后（输入类似 15:44）,当前时间为10:00那么就返回false
func (self *TimeFunc) NowIsAfter(timepoint string) (bool, error) {
	layout := "15:04"
	//common.Logger.Debug(self.Point)
	_time, err := time.Parse(layout, timepoint)
	if err != nil {
		return false, err
	}
	nowstr := time.Now().Format(layout)
	now, _ := time.Parse(layout, nowstr)
	yes := now.After(_time)
	return yes, nil
}

// 当前时间是否在输入的时间点之后（输入类似 15:44）,当前时间为10:00那么就返回false
func (self *TimeFunc) NowIsBefore(timepoint string) (bool, error) {
	layout := "15:04"
	//common.Logger.Debug(self.Point)
	_time, err := time.Parse(layout, timepoint)
	if err != nil {
		return false, err
	}
	nowstr := time.Now().Format(layout)
	now, _ := time.Parse(layout, nowstr)
	yes := now.Before(_time)
	return yes, nil
}

// ParseTime 时间字符串转换为时间
func (self *TimeFunc) ParseTime(timeStr string) time.Time {
	t, err := time.ParseInLocation("2006-01-02 15:04:05", timeStr, time.Local)
	if err != nil {
		panic(err)
	}
	return t
}

// SecToTime 将秒转换为时间多用于视频播放
func (self *TimeFunc) SecToTime(sec int64) string {
	hour := sec / 3600
	min := (sec - hour*3600) / 60
	sec = sec - hour*3600 - min*60
	return fmt.Sprintf("%02d:%02d:%02d", hour, min, sec)
}
