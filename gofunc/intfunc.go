package gofunc

type IntFunc struct {
}

func (self *IntFunc) Uint16ToByte(in uint16) []byte {
	result := []byte{}
	result = append(result, byte(in>>8))
	result = append(result, byte(in))
	return result
}
