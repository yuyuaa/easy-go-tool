package imgdraw

import (
	"embed"
	"fmt"
	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"github.com/llgcode/draw2d/draw2dimg"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
	"image"
	"image/color"
)

//go:embed extfile
var extfile embed.FS

type ImgDrawFunc struct {
	fontIns *truetype.Font
}

//var _ImgDrawFunc ImgDrawFunc

//func init() {
//	_ImgDrawFunc.getFontFamily()
//}

func (self *ImgDrawFunc) GetFontFamily() (*truetype.Font, error) {
	return self.getFontFamily("extfile/msyh.ttc")
}

// 使用粗体的微软雅黑
func (self *ImgDrawFunc) GetFontFamilyBold() (*truetype.Font, error) {
	return self.getFontFamily("extfile/msyhbold.ttc")
}

func (self *ImgDrawFunc) getFontFamily(ttcfile string) (*truetype.Font, error) {
	// 这里需要读取中文字体，否则中文文字会变成方格
	//fontBytes, err := ioutil.ReadFile("./lib/msyh.ttc")
	fontBytes, err := extfile.ReadFile(ttcfile)
	if err != nil {
		fmt.Println("read file error:", err)
		return &truetype.Font{}, err
	}

	f, err := freetype.ParseFont(fontBytes)
	if err != nil {
		fmt.Println("parse font error:", err)
		return &truetype.Font{}, err
	}
	self.fontIns = f
	return f, err
}

func (self *ImgDrawFunc) Img2RGBA(m1 image.Image) *image.RGBA {
	// 复制位rgb模式
	m := image.NewRGBA(m1.Bounds())
	for y := 0; y < m.Bounds().Dy(); y++ {
		for x := 0; x < m.Bounds().Dx(); x++ {
			m.Set(x, y, m1.At(x, y))
		}
	}
	return m
}

// 画矩形
func (self *ImgDrawFunc) DrawRect(m *image.RGBA, x1, y1, x2, y2 float64) *image.RGBA {
	fmt.Println(x1, y1, x2, y2)
	if x1 < 0 {
		x1 = 0
	}
	if y1 < 0 {
		y1 = 0
	}
	gc := draw2dimg.NewGraphicContext(m)
	gc.SetStrokeColor(color.RGBA{255, 0, 0, 0xff})
	gc.SetFillColor(color.RGBA{0, 0, 0, 0}) //矩形里面填充为透明
	gc.SetLineWidth(5)
	gc.BeginPath() // 清空
	//gc.MoveTo(80, 80) // 左上角
	//gc.LineTo(800, 80)
	//gc.LineTo(800, 800)
	//gc.LineTo(80, 800)
	//gc.LineTo(80, 80)
	gc.MoveTo(x1, y1) // 左上角
	gc.LineTo(x2, y1)
	gc.LineTo(x2, y2)
	gc.LineTo(x1, y2)
	gc.LineTo(x1, y1)
	gc.Close()
	gc.FillStroke()

	return m
	//draw2dimg.SaveToPngFile("./aaccc.png",m)
}

func (self *ImgDrawFunc) DrawRectUseImg(m1 image.Image, x1, y1, x2, y2 float64) *image.RGBA {
	m := self.Img2RGBA(m1)
	return self.DrawRect(m, x1, y1, x2, y2)
}

// 写文字,这里用了内置字体，如果想用外部字体用freetype
func (self *ImgDrawFunc) DrawText(m *image.RGBA, x, y int, label string) {
	col := color.RGBA{200, 100, 0, 255}
	point := fixed.Point26_6{fixed.Int26_6(x * 64), fixed.Int26_6(y * 64)}

	d := &font.Drawer{
		Dst:  m,
		Src:  image.NewUniform(col),
		Face: basicfont.Face7x13,
		Dot:  point,
	}
	d.DrawString(label)
}

func (self *ImgDrawFunc) DrawText1(m *image.RGBA, x, y int, label string, fontsize int) {
	self.DrawText12(m, x, y, label, fontsize, color.RGBA{R: 255, G: 255, B: 255, A: 255})
}

// 写文字,这里用了内置字体，如果想用外部字体用freetype
func (self *ImgDrawFunc) DrawText12(m *image.RGBA, x, y int, label string, fontsize int, txtcolor color.RGBA) {
	if x < 0 {
		x = 1
	}
	if y < 0 {
		y = 1
	}
	////拷贝一个字体文件到运行目录
	//fontBytes, err := ioutil.ReadFile("simsun.ttc")
	//if err != nil {
	//	panic(err)
	//}
	//font, err := freetype.ParseFont(fontBytes)
	//if err != nil {
	//	log.Println(err)
	//}
	f := freetype.NewContext()
	f.SetDPI(72) // 设置屏幕每英寸的分辨率
	f.SetFont(self.fontIns)
	//fontsize := 24
	f.SetFontSize(float64(fontsize))
	f.SetClip(m.Bounds()) // 背景
	// 设置目标图像
	f.SetDst(m)

	f.SetSrc(image.NewUniform(txtcolor)) // 白色
	//f.SetSrc(image.NewUniform(color.RGBA{R: 255, G: 0, B: 0, A: 255}))

	//pt := freetype.Pt(m.Bounds().Dx()-200, m.Bounds().Dy()-12)
	// utf8.RuneCountInString(drawStr) 获取字符串的实际大小，而不是以byte算
	pt := freetype.Pt(x, y)
	//pt := freetype.Pt(m.Bounds().Dx()-200, m.Bounds().Dy()-12)
	_, err := f.DrawString(label, pt)
	if err != nil {
		panic(err)
	}

}
