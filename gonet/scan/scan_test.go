package scan

import (
	"fmt"
	"testing"
	"time"
)

// 测试端口扫描
// ip 192.68.0.1-255 或xxx.com
// 端口 80,22,110,3301-3310
func TestScanPort(t *testing.T) {

	scanIP := NewScanIp(2, 10, false)
	// ip 192.68.0.1-255 或xxx.com
	ips, err := scanIP.GetAllIp("10.33.63.2-254")
	if err != nil {
		fmt.Printf("  ip解析出错....  %v \n", err.Error())
		return
	}
	for i := 0; i < len(ips); i++ {

		ports := scanIP.GetIpOpenPort(ips[i], "80")
		if len(ports) > 0 {
			fmt.Printf("%v【%v】开放:%v \n", time.Now().Format("2006-01-02 15:04:05"), ips[i], ports)
		}
	}
}
