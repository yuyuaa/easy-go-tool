package ycapi

import (
	"errors"
	//	"time"
)

/*
#cgo CFLAGS: -I .
#cgo LDFLAGS: -L ${SRCDIR} -lycapic
#include "ycapic.h"
*/
import "C"

//初始化看门狗
//输入值为0-30 秒
func SetWDog(interval int) error {
	if interval < 0 || interval > 30 {
		return errors.New("不在设置范围")
	}
	C.SetWDog(C.int(interval))
	return nil
}

// 启动看门狗
func StartWDog() {
	C.StartWDog()
}

//喂狗
func FeedWDog() {
	C.FeedWDog()
}

//停止
func StopWDog() {
	C.StopWDog()
}
